use crate::errors::VisageError;
use serde::Deserialize;
use strum_macros::EnumString;

#[derive(Clone, Debug, PartialEq, EnumString, strum_macros::ToString, Deserialize)]
#[strum(serialize_all = "kebab-case")]
#[serde(rename_all(deserialize = "kebab-case"))]
pub enum SSLModeType {
    Disable,
    Allow,
    Prefer,
    Require,
    VerifyCa,
    VerifyFull,
}

#[derive(Debug, Clone, Deserialize)]
pub struct ConfigurationRedis {
    pub host: String,
    pub port: u32,
    pub user: Option<String>,
    pub password: Option<String>,
    pub database: Option<String>,
    pub min_connections: Option<u32>,
    pub max_connections: u32,
    pub connection_timeout: u64,
    pub sslmode: Option<SSLModeType>,
    pub sslcert: Option<String>,
    pub sslkey: Option<String>,
    pub sslrootcert: Option<String>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct ConfigurationDatabase {
    pub host: String,
    pub port: u32,
    pub user: String,
    pub password: String,
    pub database: String,
    pub connection_number: String,
    pub read_step: u32,
    pub tmp_backlog_lifespan: u64,
    pub sslmode: Option<SSLModeType>,
    pub sslcert: Option<String>,
    pub sslkey: Option<String>,
    pub sslrootcert: Option<String>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct ConfigurationMaster {
    pub enable: bool,
}

#[derive(Debug, Clone, Deserialize)]
pub struct ConfigurationSlave {
    pub enable: bool,
}

#[derive(Debug, Clone, Deserialize)]
pub struct ConfigurationRates {
    pub ttl_lease: i64,
    pub tick_rate: i64,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Configuration {
    pub redis: ConfigurationRedis,
    pub database: ConfigurationDatabase,
    pub rates: ConfigurationRates,
    pub master: ConfigurationMaster,
    pub slave: ConfigurationSlave,
}

/// Get the configuration from the path and the environment
pub fn get(path: &std::path::Path, env_var: &str) -> Result<Configuration, VisageError> {
    let mut settings = config::Config::default();
    settings
        .merge(config::File::from(path))?
        .merge(config::Environment::with_prefix(env_var).separator("__"))?;
    Ok(settings.try_into::<Configuration>()?)
}

/// Get the configuration from the path and the environment (unparsed)
pub fn get_unparsed(path: &std::path::Path, env_var: &str) -> Result<config::Config, VisageError> {
    let mut settings = config::Config::default();
    settings
        .merge(config::File::from(path))?
        .merge(config::Environment::with_prefix(env_var).separator("__"))?;
    Ok(settings)
}
