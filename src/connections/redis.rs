use crate::configuration::ConfigurationRedis;
use crate::errors::VisageError;
use log::info;
use redis::RedisError;

/// Get a redis client from a visage configuration
///
/// # Arguments
/// * `conf` - The redis configuration
///
/// # Return value
/// The redis client
pub fn get_client(conf: &ConfigurationRedis) -> redis::RedisResult<redis::Client> {
    info!("Connecting to redis...");
    let mut connection_string = String::from("redis://");
    if conf.password.is_some() {
        connection_string.push_str(
            format!(
                "{}:{}@{}:{}",
                conf.user.as_deref().unwrap_or_default(),
                conf.password.as_deref().unwrap_or_default(),
                conf.host,
                conf.port
            )
            .as_str(),
        );
    } else {
        connection_string.push_str(format!("{}:{}", conf.host, conf.port).as_str());
    }
    if conf.database.is_some() {
        connection_string.push_str(format!("/{}", conf.database.as_deref().unwrap()).as_str());
    }
    let client = redis::Client::open(connection_string.as_str())?;
    Ok(client)
}

/// Get a redis connection from a visage configuration
///
/// # Arguments
/// * `conf` - The redis configuration
///
/// # Return value
/// The redis connection
pub async fn get_redis_connection(
    conf: &ConfigurationRedis,
) -> redis::RedisResult<(redis::Client, redis::aio::ConnectionManager)> {
    info!("Connecting to redis...");
    let client = get_client(&conf)?;
    let conn = client.get_tokio_connection_manager().await?;
    Ok((client, conn))
}

/// Get a redis connection pool from a visage configuration
///
/// # Arguments
/// * `conf` - The redis configuration
///
/// # Return value
/// The redis connection pool
pub fn get_redis_connection_pool(
    conf: &ConfigurationRedis,
) -> Result<r2d2::Pool<redis::Client>, VisageError> {
    info!("Connecting to redis...");
    let client: redis::Client = get_client(&conf)?;
    let pool: r2d2::Pool<redis::Client> = r2d2::Pool::builder()
        .connection_timeout(std::time::Duration::from_millis(conf.connection_timeout))
        .min_idle(conf.min_connections)
        .max_size(conf.max_connections)
        .build(client)?;
    Ok(pool)
}

/// Test a redis connection to see if the server is alive
///
/// # Arguments
/// * `conn` - The redis connection
///
/// # Return value
/// Nothing
pub async fn test_connection(conn: &mut redis::aio::ConnectionManager) -> Result<(), RedisError> {
    redis::cmd("PING").query_async(conn).await?;
    Ok(())
}
