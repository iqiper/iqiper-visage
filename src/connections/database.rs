use crate::configuration::{ConfigurationDatabase, SSLModeType};
use diesel::r2d2::{ConnectionManager, Pool};
use log::info;

/// Build a database pool from a visage configuration
///
/// # Arguments
/// * `conf` - The database configuration
///
/// # Return value
/// The database pool
pub async fn get_db_pool(
    conf: &ConfigurationDatabase,
) -> Result<r2d2::Pool<diesel::r2d2::ConnectionManager<diesel::PgConnection>>, r2d2::Error> {
    info!("Initializing database connection pool...");
    let mut connection_string = format!(
        "postgresql://{}:{}@{}:{}/{}?sslmode={}",
        conf.user,
        conf.password,
        conf.host,
        conf.port,
        conf.database,
        conf.sslmode
            .as_ref()
            .unwrap_or(&SSLModeType::Prefer)
            .to_string()
    );
    if let Some(sslcert) = &conf.sslcert {
        connection_string.push_str(format!("&sslcert={}", sslcert).as_str());
    }
    if let Some(sslkey) = &conf.sslkey {
        connection_string.push_str(format!("&sslkey={}", sslkey).as_str());
    }
    if let Some(sslrootcert) = &conf.sslrootcert {
        connection_string.push_str(format!("&sslrootcert={}", sslrootcert).as_str());
    }
    let manager = ConnectionManager::new(connection_string);
    let db_pool = Pool::builder()
        .error_handler(Box::new(diesel::r2d2::LoggingErrorHandler {}))
        .event_handler(Box::new(db::events_logger::IqiperDatabaseEventHandler {}))
        .max_size(conf.connection_number.parse::<u32>().unwrap_or(16))
        .build(manager)?;
    Ok(db_pool)
}
