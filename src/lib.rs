//! `iqiper-visage` is a boilerplate lib used to expose the service in a `redis` server.
//!
//! ## Master-Slave
//!
//! Some service require a master-slave architecture. The master usually has the role
//! of distributing the tasks and looking over the slave if some were to fail.
//!
//! In case of failure the master would redistribute tasks to the live slaves.
//!
//! ## Lease
//!
//! The main objective of this crate is to maintain the "lease" of the running service.
//! This lease insure that the server is still alive. It's a simple key in `redis`
//! with an expiration date. If the server didn't update its lease in X seconds, it can
//! be considered in a dead state. The master `visage` can now handle if there are some
//! jobs to redistributes.
//!
//! ## Connection
//!
//! This crate also provide some function to connect to the database and `redis`.
//! This make a standard way to register a service in `redis` and to get a database
//! connection. It keeps those service as DRY as possible.

pub mod configuration;
pub mod connections;
pub mod errors;
mod visage;

pub use visage::{Visage, VisageRole};

pub extern crate r2d2;
pub extern crate redis;
