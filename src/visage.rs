use crate::configuration::Configuration;
use crate::connections;
use crate::errors::VisageError;
use getset::{Getters, MutGetters, Setters};
use log::{debug, error, info, warn};
use std::fmt::Debug;
use std::str::FromStr;
use std::sync::{Arc, RwLock};
use strum_macros::Display;
use strum_macros::EnumString;
use uuid::Uuid;

#[derive(Debug, Clone, Display, EnumString)]
pub enum VisageRole {
    #[strum(serialize = "M")]
    Master,
    #[strum(serialize = "S")]
    Slave,
    #[strum(serialize = "B")]
    Both,
}

#[macro_export]
macro_rules! VISAGE_ID_FORMAT {
    ($visage_id:expr) => {
        format!("V_{}", $visage_id)
    };
}

#[macro_export]
macro_rules! VISAGE_TMP_FORMAT {
    ($oeil_id:expr) => {
        format!("TMP_{}", $oeil_id)
    };
}

#[derive(Getters, MutGetters, Setters)]
pub struct Visage {
    #[getset(get = "pub")]
    client: redis::Client,
    #[getset(get = "pub")]
    config: Configuration,
    #[getset(get = "pub")]
    redis_conn: Arc<RwLock<redis::aio::ConnectionManager>>,
    #[getset(get = "pub")]
    db_conn: r2d2::Pool<diesel::r2d2::ConnectionManager<diesel::PgConnection>>,
    #[getset(get = "pub", set = "pub")]
    id: Uuid,
    #[getset(get = "pub")]
    role: VisageRole,
    #[getset(get = "pub")]
    registrar_name: &'static str,
}

impl Visage {
    /// Create a new visage object
    ///
    /// # Arguments
    /// * `config` - The configuration object
    /// * `registrar_name` - The name of the registrar in which to set the lease
    ///
    /// # Return value
    /// The Visage object
    pub async fn new(
        config: Configuration,
        registrar_name: &'static str,
    ) -> Result<Self, VisageError> {
        let (client, mut redis_conn) =
            connections::redis::get_redis_connection(&config.redis).await?;
        let db_conn = connections::database::get_db_pool(&config.database).await?;
        connections::redis::test_connection(&mut redis_conn).await?;
        let role = Visage::get_role(&config);
        Ok(Visage {
            id: Uuid::new_v4(),
            client,
            config,
            db_conn,
            redis_conn: Arc::new(RwLock::new(redis_conn)),
            role,
            registrar_name,
        })
    }

    /// Register the visage lease in the registrar
    ///
    /// # Return value
    /// The id of the visage that was just registered
    pub async fn register_lease(&mut self) -> Result<Uuid, VisageError> {
        let mut visage_id: Uuid;
        while {
            visage_id = Uuid::new_v4();
            let score: &str;
            if let VisageRole::Master = self.role() {
                score = "+inf";
            } else {
                score = "0";
            };
            let nb: (u64, bool) = self
                .apply_pipe_in_redis(
                    redis::pipe()
                        // Add to the `Y` list
                        .zadd(*self.registrar_name(), visage_id.to_string(), score)
                        // Set the role and expiring record
                        .cmd("SET")
                        .arg(VISAGE_ID_FORMAT!(visage_id))
                        .arg(self.role().to_string())
                        .arg("EX")
                        .arg(self.config().rates.ttl_lease)
                        .arg("NX"),
                )
                .await?; //TODO Handle error on duplicate
            nb.0 == 0 || nb.1 != true
        } {}
        self.set_id(visage_id);
        Ok(*(self.id()))
    }

    /// Apply a pipeline of command in `redis`
    ///
    /// # Arguments
    /// * `pipeline` - The `redis` pipeline
    ///
    /// # Return value
    /// The redis response to the pipeline
    pub async fn apply_pipe_in_redis<R: redis::FromRedisValue>(
        &self,
        pipeline: &mut redis::Pipeline,
    ) -> Result<R, VisageError> {
        let mut conn = self
            .redis_conn()
            .write()
            .map_err(|_e| VisageError::LockPoisonError(String::from("redis connection")))?;
        Ok(pipeline.query_async(&mut *conn).await?)
    }

    /// Apply a command in the redis pipeline
    ///
    /// # Arguments
    /// * `cmd` - The `redis` command
    ///
    /// # Return value
    /// The redis response to the command
    pub async fn apply_cmd_in_redis<R: redis::FromRedisValue>(
        &self,
        cmd: &mut redis::Cmd,
    ) -> Result<R, VisageError> {
        let mut conn = self
            .redis_conn()
            .write()
            .map_err(|_e| VisageError::LockPoisonError(String::from("redis connection")))?;
        Ok(cmd.query_async(&mut *conn).await?)
    }

    /// Figure out which role this `visage` is supposed to run as from the configuration
    ///
    /// # Arguments
    /// * `config` - The configuration object
    ///
    /// # Return value
    /// The role
    pub fn get_role(config: &Configuration) -> VisageRole {
        if config.master.enable && !config.slave.enable {
            VisageRole::Master
        } else if !config.master.enable && config.slave.enable {
            VisageRole::Slave
        } else {
            VisageRole::Both
        }
    }

    /// Get the visage with the least charge
    ///
    /// # Return value
    /// The uuid of the lightest visage, None if there is no Visage registred in the registrar
    pub async fn get_lightest_visage(&self) -> Result<Option<Uuid>, VisageError> {
        loop {
            let lightest_unparsed: Vec<String> = self
                .apply_cmd_in_redis(
                    &mut redis::cmd("ZRANGEBYSCORE")
                        .arg(*self.registrar_name())
                        .arg("-inf")
                        .arg("+inf")
                        .arg("LIMIT")
                        .arg(0)
                        .arg(1),
                )
                .await?;
            if lightest_unparsed.len() == 0 {
                return Ok(None);
            }
            let id = match Uuid::from_str(lightest_unparsed[0].as_str()) {
                Ok(res) => res,
                Err(_e) => {
                    self.apply_cmd_in_redis(&mut redis::Cmd::zrem(
                        *self.registrar_name(),
                        lightest_unparsed,
                    ))
                    .await?;
                    continue;
                }
            };
            let role = self.get_visage_role(&id).await?;
            if let Some(VisageRole::Master) = role {
                warn!(
                    "The master {} should be available, maybe the slave(s) is down ?",
                    id
                );
                continue;
            } else if let None = role {
                warn!("Undefined role for {}, deleting", id);
                self.apply_pipe_in_redis(
                    &mut redis::pipe()
                        .zrem(*self.registrar_name(), id.to_string())
                        .del(VISAGE_ID_FORMAT!(id)),
                )
                .await?;
                continue;
            }
            return Ok(Some(id));
        }
    }

    /// Get the score of the visage
    ///
    /// # Arguments
    /// * `visage_id` - The visage for which to get the score
    ///
    /// # Return value
    /// The score if any
    pub async fn get_visage_score(&self, visage_id: &Uuid) -> Result<Option<usize>, VisageError> {
        let record: Option<usize> = self
            .apply_cmd_in_redis(&mut redis::Cmd::zscore(
                *self.registrar_name(),
                visage_id.to_string(),
            ))
            .await?;
        Ok(record)
    }
    /// Get the role of the visage
    ///
    /// # Arguments
    /// * `visage_id` - The visage for which to get the score
    ///
    /// # Return value
    /// The role, if any
    pub async fn get_visage_role(
        &self,
        visage_id: &Uuid,
    ) -> Result<Option<VisageRole>, VisageError> {
        let record: Option<String> = self
            .apply_cmd_in_redis(&mut redis::Cmd::get(VISAGE_ID_FORMAT!(visage_id)))
            .await?;
        match record {
            Some(record) => Ok(VisageRole::from_str(record.as_str()).ok()),
            None => Ok(None),
        }
    }

    /// Reset the expiration date on the lease of this visage
    ///
    /// # Return value
    /// The uuid of the visage
    pub async fn update_lease(&self) -> Result<Uuid, VisageError> {
        let visage_id = *self.id();
        debug!("Updating visage registration {}...", visage_id);
        let nb: (bool, Option<usize>) = self
            .apply_pipe_in_redis(
                &mut redis::pipe()
                    .cmd("SET")
                    .arg(VISAGE_ID_FORMAT!(visage_id))
                    .arg(self.role().to_string())
                    .arg("EX")
                    .arg(self.config().rates.ttl_lease as usize)
                    .arg("XX")
                    .zrank(*self.registrar_name(), self.id().to_string()),
            )
            .await?;
        if nb.0 == false {
            error!("Failed to update visage registration");
            return Err(VisageError::RegistrationFailed);
        }
        match nb.1 {
            Some(_x) => (),
            None => {
                error!("The visage has been deleted from the `YEUX` list. Going to die...");
                return Err(VisageError::RegistrationFailed);
            }
        }
        Ok(visage_id)
    }

    /// Unregister the lease of the visage
    ///
    /// # Arguments
    /// * `id` - Unregister the visage with id, or this visage if None
    ///
    /// # Return value
    /// Nothing
    pub async fn unregister_lease(&self, id: Option<Uuid>) -> Result<(), VisageError> {
        let visage_id = self.id().clone();
        let id = id.unwrap_or_else(|| visage_id);
        info!("Unregistering {}...", id);
        let nb: (bool, bool) = self
            .apply_pipe_in_redis(
                redis::pipe()
                    .zrem(*self.registrar_name(), id.to_string())
                    .del(VISAGE_ID_FORMAT!(id)),
            )
            .await?;
        if !nb.0 {
            error!("Unabled to remove visage {} from registrar set", id);
        }
        Ok(())
    }

    /// Check for dead visage
    ///
    /// # Return value
    /// A list of dead visage id
    pub async fn check_orphaned_lease(&self) -> Result<Vec<Uuid>, VisageError> {
        let yeux: Vec<String> = self
            .apply_cmd_in_redis(
                redis::cmd("ZRANGE")
                    .arg(*self.registrar_name())
                    .arg(0 as usize)
                    .arg(-1 as i64),
            ) // TODO Use ZSCAN
            .await?;
        let mut check_command = redis::pipe();
        for visage_id in yeux.iter() {
            check_command.exists(VISAGE_ID_FORMAT!(visage_id));
        }
        let existing_visage_map: Vec<bool> = self.apply_pipe_in_redis(&mut check_command).await?;
        let mut res: Vec<Uuid> = vec![];
        for (visage_id_formatted, exists) in yeux.iter().zip(existing_visage_map.iter()) {
            if !exists {
                match Uuid::from_str(visage_id_formatted) {
                    Ok(id) => res.push(id),
                    Err(_e) => {
                        self.apply_cmd_in_redis(&mut redis::Cmd::zrem(
                            *self.registrar_name(),
                            visage_id_formatted,
                        ))
                        .await?
                    }
                };
            }
        }
        for visage_id in res.iter() {
            self.unregister_lease(Some(*visage_id)).await?;
        }
        if res.len() > 0 {
            info!(
                "Delete {} visage(s){}{}",
                res.len(),
                match res.len() {
                    0 => "",
                    _ => " :\n\t- ",
                },
                res.iter()
                    .map(|x| x.to_string())
                    .collect::<Vec<String>>()
                    .join("\n\t- ")
            );
        }
        Ok(res)
    }

    /// Rename a key to a random, free key
    ///
    /// # Arguments
    /// * `key` - The key to rename
    ///
    /// # Return value
    /// The new key name if the rename was successful
    pub async fn rename_to_random(&self, key: String) -> Result<Option<String>, VisageError> {
        let mut new_name: String;
        let exists: bool = self
            .apply_cmd_in_redis(&mut redis::Cmd::exists(key.as_str()))
            .await?;
        if !exists {
            return Ok(None);
        }
        while {
            new_name = VISAGE_TMP_FORMAT!(Uuid::new_v4().to_string());
            let res: bool = self
                .apply_cmd_in_redis(&mut redis::Cmd::rename_nx(key.as_str(), new_name.as_str()))
                .await?;
            !res
        } {}
        let exists: bool = self
            .apply_cmd_in_redis(&mut redis::Cmd::exists(new_name.as_str()))
            .await?;
        if exists {
            self.apply_cmd_in_redis(&mut redis::Cmd::expire(
                new_name.as_str(),
                self.config().rates.ttl_lease as usize,
            ))
            .await?;
        }
        Ok(Some(new_name))
    }
}
