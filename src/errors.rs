/// Wrap all kind of error that could be encoutered by the service.
#[derive(Debug)]
pub enum VisageError {
    RedisError(redis::RedisError),
    /// Error occuring when building database connection pool
    R2D2Error(r2d2::Error),
    /// Error occuring when parsing the configuration
    ConfigError(config::ConfigError),
    /// Error occuring when trying to parse a faulty uuid
    UuidError(uuid::Error),
    /// Error occuring when a database function fails
    LockPoisonError(String),
    // When a visage fail to register itself
    RegistrationFailed,
}

impl std::fmt::Display for VisageError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            VisageError::RedisError(err) => write!(f, "RedisError : {}", err),
            VisageError::R2D2Error(err) => write!(f, "R2D2Error : {}", err),
            VisageError::UuidError(err) => write!(f, "UuidError : {}", err),
            VisageError::ConfigError(err) => write!(f, "ConfigError : {}", err),
            VisageError::LockPoisonError(err) => {
                write!(f, "LockPoisonError : {} lock is poisoned.", err)
            }
            VisageError::RegistrationFailed => {
                write!(f, "Failed to register the `visage` in redis")
            }
        }
    }
}

impl std::error::Error for VisageError {}

impl VisageError {
    pub fn from_poisoned_lock(lock: String) -> Self {
        VisageError::LockPoisonError(lock)
    }
}

impl From<redis::RedisError> for VisageError {
    fn from(err: redis::RedisError) -> Self {
        VisageError::RedisError(err)
    }
}

impl From<r2d2::Error> for VisageError {
    fn from(err: r2d2::Error) -> Self {
        VisageError::R2D2Error(err)
    }
}

impl From<uuid::Error> for VisageError {
    fn from(err: uuid::Error) -> Self {
        VisageError::UuidError(err)
    }
}

impl From<config::ConfigError> for VisageError {
    fn from(err: config::ConfigError) -> Self {
        VisageError::ConfigError(err)
    }
}
