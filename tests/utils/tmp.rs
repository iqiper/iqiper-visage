extern crate iqiper_visage;
// importing common module.
use super::*;
use iqiper_visage::Visage;

#[tokio::test]
async fn none() {
    let mut visage_test = setup().await;
    visage_test.config.master.enable = true;
    visage_test.config.rates.ttl_lease = 60;
    let master_visage = Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
        .await
        .expect("The visage should've been created");
    let res = master_visage
        .rename_to_random(String::from("toto"))
        .await
        .expect("Cleaning orphaned visage failed");
    assert_eq!(res.is_none(), true, "It should be none");
    clean_up(&mut visage_test).await;
}

#[tokio::test]
async fn some() {
    let mut visage_test = setup().await;
    visage_test.config.master.enable = true;
    visage_test.config.rates.ttl_lease = 60;
    let master_visage = Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
        .await
        .expect("The visage should've been created");
    let _e: bool = redis::Cmd::set("toto", "tutu")
        .query_async(&mut visage_test.test_conn)
        .await
        .unwrap();
    let new_key = master_visage
        .rename_to_random(String::from("toto"))
        .await
        .expect("Cleaning orphaned visage failed");
    assert_eq!(new_key.is_some(), true, "It should be some");
    let res: Option<String> = redis::Cmd::get("toto")
        .query_async(&mut visage_test.test_conn)
        .await
        .unwrap();
    assert_eq!(res.is_none(), true, "should be none");
    let res: Option<String> = redis::Cmd::get(new_key)
        .query_async(&mut visage_test.test_conn)
        .await
        .unwrap();
    assert_eq!(res.is_some(), true, "should be some");
    assert_eq!(res.unwrap(), String::from("tutu"), "key mismatch");
    clean_up(&mut visage_test).await;
}
