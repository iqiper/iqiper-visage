extern crate iqiper_visage;
// importing common module.
use super::*;
use iqiper_visage::Visage;

#[tokio::test]
async fn none() {
    let mut visage_test = setup().await;
    visage_test.config.master.enable = true;
    visage_test.config.rates.ttl_lease = 60;
    let master_visage = Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
        .await
        .expect("The visage should've been created");
    let res = master_visage
        .get_visage_score(master_visage.id())
        .await
        .expect("get the oeil score");

    assert_eq!(res.is_none(), true, "It should be none");
    clean_up(&mut visage_test).await;
}

#[tokio::test]
async fn single_self() {
    let mut visage_test = setup().await;
    visage_test.config.master.enable = true;
    visage_test.config.rates.ttl_lease = 60;
    let mut master_visage = Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
        .await
        .expect("The visage should've been created");
    master_visage
        .register_lease()
        .await
        .expect("to have registered the visage");
    let res = master_visage
        .get_visage_score(master_visage.id())
        .await
        .expect("get the oeil score");

    assert_eq!(res.is_some(), true, "It should be some");
    assert_eq!(res.unwrap(), 0, "It should be 0");
    clean_up(&mut visage_test).await;
}

#[tokio::test]
async fn single_other() {
    let mut visage_test = setup().await;
    visage_test.config.master.enable = true;
    visage_test.config.rates.ttl_lease = 60;
    let mut master_visage = Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
        .await
        .expect("The visage should've been created");
    master_visage
        .register_lease()
        .await
        .expect("to have registered the visage");
    visage_test.config.master.enable = false;
    let mut slave_oeil = Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
        .await
        .expect("The visage should've been created");
    slave_oeil
        .register_lease()
        .await
        .expect("to have registered the visage");
    let res = master_visage
        .get_visage_score(slave_oeil.id())
        .await
        .expect("get the oeil score");

    assert_eq!(res.is_some(), true, "It should be some");
    assert_eq!(res.unwrap(), 0, "It should be 0");
    clean_up(&mut visage_test).await;
}
