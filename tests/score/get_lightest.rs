extern crate iqiper_visage;
// importing common module.
use super::*;
use iqiper_visage::Visage;

#[tokio::test]
async fn none() {
    let mut visage_test = setup().await;
    visage_test.config.master.enable = true;
    visage_test.config.rates.ttl_lease = 60;
    let master_visage = Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
        .await
        .expect("The visage should've been created");
    let res = master_visage
        .get_lightest_visage()
        .await
        .expect("Cleaning orphaned visage failed");

    assert_eq!(res.is_none(), true, "It should be none");
    clean_up(&mut visage_test).await;
}

#[tokio::test]
async fn single() {
    let mut visage_test = setup().await;
    visage_test.config.master.enable = true;
    visage_test.config.rates.ttl_lease = 60;
    let mut master_visage = Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
        .await
        .expect("The visage should've been created");
    master_visage
        .register_lease()
        .await
        .expect("to have registered the visage");
    let res = master_visage
        .get_lightest_visage()
        .await
        .expect("Cleaning orphaned visage failed");

    assert_eq!(res.is_some(), true, "It should be none");
    assert_eq!(res.unwrap(), *master_visage.id(), "Ids mismatch");
    clean_up(&mut visage_test).await;
}

#[tokio::test]
async fn multiple() {
    let mut visage_test = setup().await;
    visage_test.config.master.enable = true;
    visage_test.config.rates.ttl_lease = 60;
    let mut master_visage = Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
        .await
        .expect("The visage should've been created");
    master_visage
        .register_lease()
        .await
        .expect("to have register the visage");
    visage_test.config.master.enable = false;
    visage_test.config.slave.enable = true;
    let mut slave_visage = vec![
        Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
            .await
            .expect("The visage should've been created"),
        Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
            .await
            .expect("The visage should've been created"),
        Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
            .await
            .expect("The visage should've been created"),
    ];
    for v in slave_visage.iter_mut() {
        v.register_lease()
            .await
            .expect("to have register the visage");
    }
    let _e: (usize, usize, usize) = redis::pipe()
        .zincr(
            *master_visage.registrar_name(),
            master_visage.id().to_string(),
            10,
        )
        .zincr(
            *master_visage.registrar_name(),
            slave_visage[0].id().to_string(),
            10,
        )
        .zincr(
            *master_visage.registrar_name(),
            slave_visage[2].id().to_string(),
            10,
        )
        .query_async(&mut visage_test.test_conn)
        .await
        .expect("The verification in redis to be fetched");
    let res = master_visage
        .get_lightest_visage()
        .await
        .expect("Cleaning orphaned visage failed");

    assert_eq!(res.is_some(), true, "It should be none");
    assert_eq!(res.unwrap(), *slave_visage[1].id(), "Ids mismatch");
    clean_up(&mut visage_test).await;
}

#[tokio::test]
async fn with_badly_formatted() {
    let mut visage_test = setup().await;
    visage_test.config.master.enable = true;
    visage_test.config.rates.ttl_lease = 60;
    let mut master_visage = Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
        .await
        .expect("The visage should've been created");
    master_visage
        .register_lease()
        .await
        .expect("to have register the visage");
    visage_test.config.master.enable = false;
    visage_test.config.slave.enable = true;
    let mut slave_visage = vec![
        Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
            .await
            .expect("The visage should've been created"),
        Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
            .await
            .expect("The visage should've been created"),
        Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
            .await
            .expect("The visage should've been created"),
    ];
    for v in slave_visage.iter_mut() {
        v.register_lease()
            .await
            .expect("to have register the visage");
    }
    let _e: usize = redis::Cmd::zadd(*master_visage.registrar_name(), "AAAAAAAAAAAAAAAAAAAA", 1)
        .query_async(&mut visage_test.test_conn)
        .await
        .unwrap();
    let _e: (usize, usize, usize, usize) = redis::pipe()
        .zincr(
            *master_visage.registrar_name(),
            master_visage.id().to_string(),
            10,
        )
        .zincr(
            *master_visage.registrar_name(),
            slave_visage[0].id().to_string(),
            10,
        )
        .zincr(
            *master_visage.registrar_name(),
            slave_visage[1].id().to_string(),
            5,
        )
        .zincr(
            *master_visage.registrar_name(),
            slave_visage[2].id().to_string(),
            10,
        )
        .query_async(&mut visage_test.test_conn)
        .await
        .unwrap();
    let res = master_visage
        .get_lightest_visage()
        .await
        .expect("Cleaning orphaned visage failed");
    assert_eq!(res.is_some(), true, "It should be none");
    assert_eq!(res.unwrap(), *slave_visage[1].id(), "Ids mismatch");
    clean_up(&mut visage_test).await;
}
