extern crate iqiper_visage;
// importing common module.
use super::*;
use iqiper_visage::{Visage, VISAGE_ID_FORMAT};

#[tokio::test]
async fn normal() {
    let mut visage_test = setup().await;
    visage_test.config.master.enable = true;
    visage_test.config.rates.ttl_lease = 60;
    let mut master_visage = Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
        .await
        .expect("The visage should've been created");
    master_visage
        .register_lease()
        .await
        .expect("Registering lease");
    let res = master_visage
        .update_lease()
        .await
        .expect("Registering lease");
    assert_eq!(res, *master_visage.id(), "ids mismatch");
    clean_up(&mut visage_test).await;
}

#[tokio::test]
async fn no_more_in_registrar() {
    let mut visage_test = setup().await;
    visage_test.config.master.enable = true;
    visage_test.config.rates.ttl_lease = 60;
    let mut master_visage = Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
        .await
        .expect("The visage should've been created");
    master_visage
        .register_lease()
        .await
        .expect("Registering lease");
    let _e: bool = redis::Cmd::zrem(TEST_REGISTRAR, master_visage.id().to_string())
        .query_async(&mut visage_test.test_conn)
        .await
        .unwrap();
    master_visage
        .update_lease()
        .await
        .expect_err("should've failed");
    clean_up(&mut visage_test).await;
}

#[tokio::test]
async fn no_more_lease() {
    let mut visage_test = setup().await;
    visage_test.config.master.enable = true;
    visage_test.config.rates.ttl_lease = 60;
    let mut master_visage = Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
        .await
        .expect("The visage should've been created");
    master_visage
        .register_lease()
        .await
        .expect("Registering lease");
    let _e: bool = redis::Cmd::del(VISAGE_ID_FORMAT!(master_visage.id()))
        .query_async(&mut visage_test.test_conn)
        .await
        .unwrap();
    master_visage
        .update_lease()
        .await
        .expect_err("should've failed");
    clean_up(&mut visage_test).await;
}
