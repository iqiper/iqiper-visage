extern crate iqiper_visage;
// importing common module.
use super::*;
use iqiper_visage::{Visage, VISAGE_ID_FORMAT};

#[tokio::test]
async fn normal() {
    let mut visage_test = setup().await;
    visage_test.config.master.enable = true;
    visage_test.config.rates.ttl_lease = 60;
    let mut master_visage = Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
        .await
        .expect("The visage should've been created");
    master_visage
        .register_lease()
        .await
        .expect("to have registered the visage");
    visage_test.config.master.enable = false;
    visage_test.config.slave.enable = true;
    master_visage
        .check_orphaned_lease()
        .await
        .expect("Cleaning orphaned visage failed");
    master_visage
        .unregister_lease(None)
        .await
        .expect("Failed to unregistered the visage");
    let res: (bool, Option<usize>) = redis::pipe()
        .exists(VISAGE_ID_FORMAT!(master_visage.id()))
        .zscore(
            *master_visage.registrar_name(),
            master_visage.id().to_string(),
        )
        .query_async(&mut visage_test.test_conn)
        .await
        .expect("Failed to check if unregistering worked");
    assert_eq!(res.0, false, "The visage object should've been deleted");
    assert_eq!(
        res.1.is_some(),
        false,
        "The visage id should've been deleted from the `yeux` set"
    );
    clean_up(&mut visage_test).await;
}

#[tokio::test]
async fn none() {
    let mut visage_test = setup().await;
    visage_test.config.master.enable = true;
    visage_test.config.rates.ttl_lease = 60;
    let master_visage = Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
        .await
        .expect("The visage should've been created");
    visage_test.config.master.enable = false;
    visage_test.config.slave.enable = true;
    master_visage
        .check_orphaned_lease()
        .await
        .expect("Cleaning orphaned visage failed");
    master_visage
        .unregister_lease(None)
        .await
        .expect("Failed to unregistered the visage");
    master_visage
        .unregister_lease(None)
        .await
        .expect("Failed to unregistered the visage");
    clean_up(&mut visage_test).await;
}
