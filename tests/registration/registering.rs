extern crate iqiper_visage;
// importing common module.
use super::*;
use iqiper_visage::{Visage, VISAGE_ID_FORMAT};

#[tokio::test]
async fn no_registrar() {
    let mut visage_test = setup().await;
    visage_test.config.master.enable = true;
    visage_test.config.rates.ttl_lease = 60;
    let master_visage = Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
        .await
        .expect("The visage should've been created");
    let res = master_visage
        .check_orphaned_lease()
        .await
        .expect("Cleaning orphaned visage failed");

    assert_eq!(res.len(), 0, "It shouldn't have deleted anything");
    clean_up(&mut visage_test).await;
}

#[tokio::test]
async fn expired_visage() {
    let mut visage_test = setup().await;
    visage_test.config.master.enable = true;
    visage_test.config.rates.ttl_lease = 60;
    let mut master_visage = Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
        .await
        .expect("The visage should've been created");
    master_visage
        .register_lease()
        .await
        .expect("to have register the visage");
    visage_test.config.master.enable = false;
    visage_test.config.slave.enable = true;
    visage_test.config.rates.ttl_lease = 1;
    let mut slave_visage = vec![
        Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
            .await
            .expect("The visage should've been created"),
        Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
            .await
            .expect("The visage should've been created"),
        Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
            .await
            .expect("The visage should've been created"),
    ];
    for v in slave_visage.iter_mut() {
        v.register_lease()
            .await
            .expect("to have register the visage");
    }
    std::thread::sleep(std::time::Duration::from_millis(1200));
    master_visage
        .check_orphaned_lease()
        .await
        .expect("Cleaning orphaned visage failed");
    let res: (
        bool,
        bool,
        bool,
        Option<usize>,
        Option<usize>,
        Option<usize>,
    ) = redis::pipe()
        .exists(VISAGE_ID_FORMAT!(slave_visage[0].id()))
        .exists(VISAGE_ID_FORMAT!(slave_visage[1].id()))
        .exists(VISAGE_ID_FORMAT!(slave_visage[2].id()))
        .zscore(
            *master_visage.registrar_name(),
            slave_visage[0].id().to_string(),
        )
        .zscore(
            *master_visage.registrar_name(),
            slave_visage[1].id().to_string(),
        )
        .zscore(
            *master_visage.registrar_name(),
            slave_visage[2].id().to_string(),
        )
        .query_async(&mut visage_test.test_conn)
        .await
        .expect("The verification in redis to be fetched");
    assert_eq!(res.0, false, "The witness key should exists");
    assert_eq!(res.1, false, "The witness key should exists");
    assert_eq!(res.2, false, "The witness key should exists");
    assert_eq!(
        res.3.is_some(),
        false,
        "The visage should exist in the the registrar sset"
    );
    assert_eq!(
        res.4.is_some(),
        false,
        "The visage should exist in the the registrar sset"
    );
    assert_eq!(
        res.5.is_some(),
        false,
        "The visage should exist in the the registrar sset"
    );
    clean_up(&mut visage_test).await;
}

#[tokio::test]
async fn not_expired_visage() {
    let mut visage_test = setup().await;
    visage_test.config.master.enable = true;
    visage_test.config.rates.ttl_lease = 60;
    let mut master_visage = Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
        .await
        .expect("The visage should've been created");
    master_visage
        .register_lease()
        .await
        .expect("to have register the visage");
    visage_test.config.master.enable = false;
    visage_test.config.slave.enable = true;
    visage_test.config.rates.ttl_lease = 10;
    let mut slave_visage = vec![
        Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
            .await
            .expect("The visage should've been created"),
        Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
            .await
            .expect("The visage should've been created"),
        Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
            .await
            .expect("The visage should've been created"),
    ];
    for v in slave_visage.iter_mut() {
        v.register_lease()
            .await
            .expect("to have register the visage");
    }
    master_visage
        .check_orphaned_lease()
        .await
        .expect("Cleaning orphaned visage failed");
    let res: (
        bool,
        bool,
        bool,
        Option<usize>,
        Option<usize>,
        Option<usize>,
    ) = redis::pipe()
        .exists(VISAGE_ID_FORMAT!(slave_visage[0].id()))
        .exists(VISAGE_ID_FORMAT!(slave_visage[1].id()))
        .exists(VISAGE_ID_FORMAT!(slave_visage[2].id()))
        .zscore(
            *master_visage.registrar_name(),
            slave_visage[0].id().to_string(),
        )
        .zscore(
            *master_visage.registrar_name(),
            slave_visage[1].id().to_string(),
        )
        .zscore(
            *master_visage.registrar_name(),
            slave_visage[2].id().to_string(),
        )
        .query_async(&mut visage_test.test_conn)
        .await
        .expect("The verification in redis to be fetched");
    assert_eq!(res.0, true, "The witness key should exists");
    assert_eq!(res.1, true, "The witness key should exists");
    assert_eq!(res.2, true, "The witness key should exists");
    assert_eq!(
        res.3.is_some(),
        true,
        "The visage should exist in the the registrar sset"
    );
    assert_eq!(
        res.4.is_some(),
        true,
        "The visage should exist in the the registrar sset"
    );
    assert_eq!(
        res.5.is_some(),
        true,
        "The visage should exist in the the registrar sset"
    );
    clean_up(&mut visage_test).await;
}

#[tokio::test]
async fn unknown_key() {
    let mut visage_test = setup().await;
    visage_test.config.master.enable = true;
    visage_test.config.rates.ttl_lease = 60;
    let mut master_visage = Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
        .await
        .expect("The visage should've been created");
    master_visage
        .register_lease()
        .await
        .expect("to have register the visage");
    visage_test.config.master.enable = false;
    visage_test.config.slave.enable = true;
    visage_test.config.rates.ttl_lease = 10;
    let mut slave_visage = vec![
        Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
            .await
            .expect("The visage should've been created"),
        Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
            .await
            .expect("The visage should've been created"),
        Visage::new(visage_test.config.clone(), TEST_REGISTRAR)
            .await
            .expect("The visage should've been created"),
    ];
    for v in slave_visage.iter_mut() {
        v.register_lease()
            .await
            .expect("to have register the visage");
    }
    let add_dummy: bool =
        redis::Cmd::zadd(*master_visage.registrar_name(), "AAAAAAAAAAAAAAAAAAAAA", 0)
            .query_async(&mut visage_test.test_conn)
            .await
            .expect("Failed to add dummy value to redis");
    assert_eq!(add_dummy, true, "Adding dummy value failed");
    master_visage
        .check_orphaned_lease()
        .await
        .expect("Cleaning orphaned visage failed");
    let res: (
        bool,
        bool,
        bool,
        Option<usize>,
        Option<usize>,
        Option<usize>,
    ) = redis::pipe()
        .exists(VISAGE_ID_FORMAT!(slave_visage[0].id()))
        .exists(VISAGE_ID_FORMAT!(slave_visage[1].id()))
        .exists(VISAGE_ID_FORMAT!(slave_visage[2].id()))
        .zscore(
            *master_visage.registrar_name(),
            slave_visage[0].id().to_string(),
        )
        .zscore(
            *master_visage.registrar_name(),
            slave_visage[1].id().to_string(),
        )
        .zscore(
            *master_visage.registrar_name(),
            slave_visage[2].id().to_string(),
        )
        .query_async(&mut visage_test.test_conn)
        .await
        .expect("The verification in redis to be fetched");
    assert_eq!(res.0, true, "The witness key should exists");
    assert_eq!(res.1, true, "The witness key should exists");
    assert_eq!(res.2, true, "The witness key should exists");
    assert_eq!(
        res.3.is_some(),
        true,
        "The visage should exist in the the registrar sset"
    );
    assert_eq!(
        res.4.is_some(),
        true,
        "The visage should exist in the the registrar sset"
    );
    assert_eq!(
        res.5.is_some(),
        true,
        "The visage should exist in the the registrar sset"
    );
    clean_up(&mut visage_test).await;
}
